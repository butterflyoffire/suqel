#!/bin/bash

# Définition des variables pour les codes de couleur
green='\033[0;32m'
nc='\033[0m' # No Color

# Fonction pour traduire une phrase
translate_phrase() {
    local phrase="$1"

    # Construire la requête JSON avec la phrase
    local request='{"translate":"/translate/","sentence":"'$phrase'","direction":"fr-ka"}'

    # Mesurer le temps de début de la traduction
    local start_time=$(date +%s.%N)

    # Envoyer la requête avec curl et extraire la traduction de la réponse JSON
    local response=$(curl -sS 'https://d2sjol2amz2ojp.cloudfront.net/translate' \
        -X POST \
        -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:122.0) Gecko/20100101 Firefox/122.0' \
        -H 'Accept: application/json, text/javascript, */*; q=0.01' \
        -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' \
        -H 'Accept-Encoding: gzip, deflate, br' \
        -H 'Content-Type: application/json; charset=utf-8' \
        -H 'Origin: https://www.tasuqilt.com' \
        -H 'Connection: keep-alive' \
        -H 'Referer: https://www.tasuqilt.com/' \
        -H 'Sec-Fetch-Dest: empty' \
        -H 'Sec-Fetch-Mode: cors' \
        -H 'Sec-Fetch-Site: cross-site' \
        -H 'TE: trailers' \
        --data-raw "$request")

    # Mesurer le temps de fin de la traduction
    local end_time=$(date +%s.%N)

    # Calculer le temps écoulé
    local elapsed_time=$(echo "$end_time - $start_time" | bc)

    # Extraire la traduction de la réponse JSON
    local translation=$(echo "$response" | jq -r '.body' | jq -r '.translation')

    # Afficher la traduction avec la couleur
    local colored_translation="${green}$translation${nc}"

    echo -e "$colored_translation"

    # Afficher le temps de traduction
    echo "Temps de traduction : $elapsed_time secondes"
}

# Fonction pour traduire des phrases à partir d'un fichier
translate_file() {
    local filename="$1"

    # Vérifier si le fichier existe
    if [ ! -f "$filename" ]; then
        echo "Le fichier $filename n'existe pas."
        return 1
    fi

    # Lire chaque ligne du fichier et traduire la phrase
    while IFS= read -r line; do
        translation=$(translate_phrase "$line" | sed 's/\x1B\[[0-9;]*[JKmsu]//g')
        echo "$line,$translation"
    done < "$filename"
}

# Vérifier si un argument est passé en ligne de commande
if [ $# -eq 0 ]; then
    # Si aucun argument n'est passé, demander à l'utilisateur s'il souhaite traduire une phrase ou un fichier
    read -p "Souhaitez-vous traduire une phrase (p) ou un fichier (f) ? : " choice

    if [ "$choice" == "p" ]; then
        # Demander à l'utilisateur de fournir la phrase à traduire
        read -p "Entrez la phrase que vous souhaitez traduire : " phrase
        translate_phrase "$phrase"
    elif [ "$choice" == "f" ]; then
        # Demander à l'utilisateur de fournir le chemin du fichier contenant les phrases à traduire
        read -p "Entrez le chemin du fichier contenant les phrases à traduire : " file_path
        translate_file "$file_path"
    else
        echo "Choix invalide. Veuillez choisir 'p' pour une phrase ou 'f' pour un fichier."
    fi
elif [ "$1" == "p" ]; then
    # Si le premier argument est 'p', demander à l'utilisateur de fournir la phrase à traduire
    read -p "Entrez la phrase que vous souhaitez traduire : " phrase
    translate_phrase "$phrase"
elif [ "$1" == "f" ]; then
    # Si le premier argument est 'f', demander à l'utilisateur de fournir le chemin du fichier contenant les phrases à traduire
    if [ -n "$2" ]; then
        translate_file "$2"
    else
        echo "Veuillez fournir le chemin du fichier contenant les phrases à traduire."
    fi
else
    echo "Usage: $0 [p|f] [file_path]"
    echo "p : traduire une phrase"
    echo "f : traduire un fichier"
fi
