# suqel

Script experimental de traduction vers la langue kabyle.

Le script effectue des requêtes sur `tasuqilt.com`.

## Pré-requis

Installer `curl`, `jq` et `bc`

`chmod u+x suqel.sh`

`./suqel.sh`

## Exemple d'utilisation

`./suqel.sh fr.txt > cible.txt`

## Usage

Usage: ./suqel4.sh [p|f] [file_path]

p : traduire une phrase

f : traduire un fichier